import math
def isPrime(number):
    if number == 1:
        return False
    for d in range(2, (round(number/2)+1)):
        if number%d == 0:
            return False
    return True
def isNise(number):
    max = len(number)
    for split in range(1, max):
        halfA = int(number[0:split])
        halfB = int(number[split:20])
        if not isPrime(halfA + halfB):
            return False
    return True

while True:
    x = input()
    if x == "0":
        break
    y = isNise(x)
    if y:
        print(x, "MAGNANIMOUS")
    else:
        print(x, "PETTY")
