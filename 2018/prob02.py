import math

def distance(v, t, a):
    v = float(v)
    t = float(t)
    a = float(a)
    return round((v * t + ( 1/2 ) * ( a * math.pow(t, 2) ) ), 3)

run = True
while run :
    ps = input()
    if(ps == "0 0 0"):
        run = False
        break
    else:
        pa = ps.split(" ")
        print(distance(pa[0], pa[2], pa[1]))
